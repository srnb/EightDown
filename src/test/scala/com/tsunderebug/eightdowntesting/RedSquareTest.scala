package com.tsunderebug.eightdowntesting

import java.awt.Color
import java.io.File
import java.util.concurrent.TimeUnit

import tf.bug.eightdown.Animation
import tf.bug.eightdown.rendering.HighQualityGIFRenderingBackend

object RedSquareTest {

  def main(args: Array[String]): Unit = {
    implicit val backend =
      HighQualityGIFRenderingBackend(Color.BLACK, transparent = true)
    val a = Animation()
    a.renderTo(256, 256, new File("blob.gif"), framerate = 10, totalTime = 8l, TimeUnit.SECONDS)
  }

}
