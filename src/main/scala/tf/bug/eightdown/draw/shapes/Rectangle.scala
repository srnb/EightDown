package tf.bug.eightdown.draw.shapes

import java.awt.Color

import tf.bug.eightdown.transform.Transformable

case class Rectangle(color: Double => Color, pos: Double => (Double, Double), w: Double => Double, h: Double => Double)
    extends Transformable {

  override def frame(t: Double): (Double, Double) => Option[Color] = {
    val (tx, ty) = pos(t)
    val tw = w(t)
    val th = h(t)
    (x, y) =>
      {
        if (x >= (tx - tw / 2) && x <= (tx + tw / 2) && y >= (ty - th / 2) && y <= (ty + th / 2)) {
          Some(color(t))
        } else {
          None
        }
      }
  }

}
