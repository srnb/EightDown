package tf.bug.eightdown.draw.shapes

import java.awt.Color

import tf.bug.eightdown.transform.Transformable

case class Ellipse(
  color: Double => Color,
  pos: Double => (Double, Double),
  w: Double => Double,
  h: Double => Double
) extends Transformable {

  override def frame(t: Double): (Double, Double) => Option[Color] = {
    val (ph, pk) = pos(t)
    val rx = w(t) / 2
    val ry = h(t) / 2
    (x, y) =>
      {
        val c = (((x - ph) * (x - ph)) / (rx * rx)) + (((y - pk) * (y - pk)) / (ry * ry))
        if (c <= 1) {
          Some(color(t))
        } else {
          None
        }
      }
  }

}
