package tf.bug.eightdown.draw

import java.awt.Color

import tf.bug.eightdown.transform.Transformable

case class Group(items: Transformable*) extends Transformable {

  override def frame(t: Double): (Double, Double) => Option[Color] = {
    val frames: Seq[(Double, Double) => Option[Color]] = items.map(_.frame(t))
    (x, y) =>
      {
        frames
          .map(_(x, y))
          .foldLeft[Option[Color]](None)(
            (o, f) =>
              o match {
                case None => f
                case Some(w) =>
                  f match {
                    case None => Some(w)
                    case Some(a) =>
                      val srcs = Seq(a.getAlpha, a.getRed, a.getGreen, a.getBlue)
                        .map(_ / 255.0)
                      val dsts = Seq(w.getAlpha, w.getRed, w.getGreen, w.getBlue)
                        .map(_ / 255.0)
                      val zips = srcs.zip(dsts)
                      val (srca, dsta) = zips.head
                      val rgbt = zips.tail
                      val na = srca + (dsta * (1 - srca))
                      val Seq(nr, ng, nb) = rgbt.map((ct) => ((ct._1 * srca) + (ct._2 * dsta * (1 - srca))) / na)
                      Some(new Color(nr.toFloat, ng.toFloat, nb.toFloat, na.toFloat))
                  }
            }
          )
      }
  }

}
