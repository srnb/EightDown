package tf.bug.eightdown.draw.asset

import java.awt.Color

import com.madgag.gif.fmsware.GifDecoder
import tf.bug.eightdown.transform.Transformable

case class AnimatedGIF(
  gd: GifDecoder,
  framecontrol: (Double, Map[Int, Int]) => Int,
  pos: Double => (Double, Double),
  w: Double => Double,
  h: Double => Double
) extends Transformable {

  val fmap = (0 until gd.getFrameCount).map(f => (f, gd.getDelay(f))).toMap

  val imap = (0 until gd.getFrameCount)
    .map(f => (f, Image(gd.getFrame(f), pos, w, h)))
    .toMap

  override def frame(t: Double): (Double, Double) => Option[Color] = {
    val cf = framecontrol(t, fmap)
    imap(cf).frame(t)
  }

}
