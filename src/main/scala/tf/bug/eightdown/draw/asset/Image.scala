package tf.bug.eightdown.draw.asset

import java.awt.Color
import java.awt.image.BufferedImage

import tf.bug.eightdown.transform.Transformable

case class Image(i: BufferedImage, pos: Double => (Double, Double), w: Double => Double, h: Double => Double)
    extends Transformable {

  override def frame(t: Double): (Double, Double) => Option[Color] = {
    val (tx, ty) = pos(t)
    val tw = w(t)
    val th = h(t)
    (x, y) =>
      {
        val nx = x - tx
        val ny = y - ty
        val sx = nx * (i.getWidth / tw)
        val sy = ny * (i.getHeight / th)
        val ax = sx + (i.getWidth / 2)
        val ay = sy + (i.getHeight / 2)
        if (ax >= 0 && ax.ceil < i.getWidth && ay >= 0 && ay.ceil < i.getHeight) {
          val ci = (ax.round, ay.round) match {
            case (`ax`, `ay`) =>
              val ca: Array[Int] = Array[Int](0, 0, 0, 0)
              i.getRaster.getPixel(ax.toInt, ay.toInt, ca)
              ca
            case (_, _) =>
              def idistance(p1: (Double, Double), p2: (Double, Double)): Double =
                1.0d / Math.sqrt((p2._1 - p1._1) * (p2._1 - p1._1) + (p2._2 - p1._2) * (p2._2 - p1._2))

              val lx = ax.floor.toInt
              val rx = ax.ceil.toInt
              val ty = ay.floor.toInt
              val by = ay.ceil.toInt
              val tla = Array[Int](0, 0, 0, 0)
              val tld = idistance((ax, ay), (lx, ty))
              val tra = Array[Int](0, 0, 0, 0)
              val trd = idistance((ax, ay), (rx, ty))
              val bla = Array[Int](0, 0, 0, 0)
              val bld = idistance((ax, ay), (lx, by))
              val bra = Array[Int](0, 0, 0, 0)
              val brd = idistance((ax, ay), (rx, by))
              val td = tld + trd + bld + brd
              i.getRaster.getPixel(lx, ty, tla)
              i.getRaster.getPixel(rx, ty, tra)
              i.getRaster.getPixel(lx, by, bla)
              i.getRaster.getPixel(rx, by, bra)
              val ar = ((tla(0) * tld) + (tra(0) * trd) + (bla(0) * bld) + (bra(0) * brd)) / td
              val gr = ((tla(1) * tld) + (tra(1) * trd) + (bla(1) * bld) + (bra(1) * brd)) / td
              val br = ((tla(2) * tld) + (tra(2) * trd) + (bla(2) * bld) + (bra(2) * brd)) / td
              val tr = ((tla(3) * tld) + (tra(3) * trd) + (bla(3) * bld) + (bra(3) * brd)) / td
              Array(ar.toInt, gr.toInt, br.toInt, tr.toInt)
          }
          val c = new Color(ci(0), ci(1), ci(2), ci(3))
          if (c.getAlpha == 0) {
            None
          } else {
            Some(c)
          }
        } else {
          None
        }
      }
  }

}
