package tf.bug.eightdown.rendering

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.{File, FileOutputStream}
import java.util.concurrent.TimeUnit

import com.madgag.gif.fmsware.AnimatedGifEncoder
import tf.bug.eightdown.draw.Drawable

case class HighQualityGIFRenderingBackend(background: Color, transparent: Boolean = false) extends RenderingBackend {

  override def render(
    w: Int,
    h: Int,
    f: File,
    d: Drawable,
    framerate: Double,
    totalTime: Long,
    unit: TimeUnit
  ): Unit = {
    val nms = unit.toMillis(totalTime)
    val os = new FileOutputStream(f)
    val e = new AnimatedGifEncoder
    e.setBackground(background)
    if (transparent) {
      e.setTransparent(background)
    }
    e.setQuality(20)
    e.setRepeat(0)
    e.start(os)
    e.setFrameRate(framerate.toFloat)
    val df = d.image(w, h)
    (BigDecimal(0.0) until nms by (1000.0 / framerate))
      .map(_ / nms)
      .foreach(
        t =>
          e.addFrame({
            val i = df(t.toDouble)
            val ni = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB)
            val ng = ni.getGraphics
            ng.setColor(background)
            ng.fillRect(0, 0, w, h)
            ng.setColor(Color.white)
            ng.drawImage(i, 0, 0, w, h, null)
            ni
          })
      )
    e.finish()
    os.close()
  }

}
