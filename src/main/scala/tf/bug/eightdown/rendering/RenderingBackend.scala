package tf.bug.eightdown.rendering

import java.io.File
import java.util.concurrent.TimeUnit

import tf.bug.eightdown.draw.Drawable

trait RenderingBackend {

  def render(w: Int, h: Int, f: File, d: Drawable, framerate: Double, totalTime: Long, unit: TimeUnit): Unit

}
