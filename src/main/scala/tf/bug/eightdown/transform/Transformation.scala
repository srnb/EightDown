package tf.bug.eightdown.transform

trait Transformation {

  def transform(t: Transformable): Transformable

}
