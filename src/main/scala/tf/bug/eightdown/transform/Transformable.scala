package tf.bug.eightdown.transform

import tf.bug.eightdown.draw.Drawable

trait Transformable extends Drawable {

  def transform(t: Transformation): Transformable = t.transform(this)

}
