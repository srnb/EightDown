package tf.bug.eightdown.transform.layerop

import java.awt.Color

import tf.bug.eightdown.draw.Drawable
import tf.bug.eightdown.transform.{Transformable, Transformation}

case class MultiplyLayerOp(mask: Drawable) extends Transformation {

  override def transform(tr: Transformable): Transformable = { t: Double =>
    {
      val trf = tr.frame(t)
      val maskf = mask.frame(t)
      (x, y) =>
        {
          (trf(x, y), maskf(x, y)) match {
            case (Some(b), Some(f)) =>
              val bfr = b.getRed
              val bfg = b.getGreen
              val bfb = b.getBlue
              val bfa = b.getAlpha
              val ffr = f.getRed / 255.0
              val ffg = f.getGreen / 255.0
              val ffb = f.getBlue / 255.0
              Some(new Color((bfr * ffr).toInt, (bfg * ffg).toInt, (bfb * ffb).toInt, bfa))
            case (Some(b), None) => Some(b)
            case (None, _) => None
          }
        }
    }
  }

}
