package tf.bug.eightdown.transform

/**
  * @param rot    rotation in Radians
  * @param center center of the rotation
  */
case class Rotation(rot: Double => Double, center: Double => (Double, Double) = _ => (0.0, 0.0))
    extends Transformation {

  override def transform(t: Transformable): Transformable =
    time => {
      val angle: Double = rot(time)
      val s = Math.sin(angle)
      val c = Math.cos(angle)
      val (cx, cy) = center(time)
      val f = t.frame(time)
      (x: Double, y: Double) =>
        {
          val nx = x - cx
          val ny = y - cy
          val tx = nx * c - ny * s
          val ty = nx * s + ny * c
          val ox = tx + cx
          val oy = ty + cy
          f(ox, oy)
        }
    }

}
