lazy val eightdown = (project in file(".")).settings(
  organization := "tf.bug",
  name := "eightdown",
  version := "0.1.0",
  scalaVersion := "2.12.8",
  resolvers += "jitpack" at "https://jitpack.io",
  libraryDependencies ++= Seq(
    "com.github.rtyley" % "animated-gif-lib-for-java" % "animated-gif-lib-1.4",
  ),
  scalacOptions ++= Seq(
    "-unchecked",
    "-deprecation",
    "-feature",
  ),
)
